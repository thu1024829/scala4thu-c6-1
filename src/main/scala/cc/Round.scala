package cc

/**
  * Created by mark on 16/04/2017.
  */
case class Round(r:Double) {
  def area()= r*r*math.Pi

  def circu= 2*r*math.Pi

}
